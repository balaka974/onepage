# AMÉNAGEMENT CONSTRUCTION 
Territiore HQE
Un territoire et un habitat repensé pour remettre l’humain et la nature ensemble

## ACtion Personnelles
- Habitat participatif
- Récupérer et réparer plutôt qu’acheter pour décorer sa maison : STOP à la sur-consommation 
- Entraide et mutualisation d’outils : “mon P’ti voisinage” un réseau pour tout partager 
- importance de la nature en ville : confort thermique et visuel  

## Domicile 
- Economie d’énergie : appareils éco, prises éco, éteindre la lumière, prendre une douche plutôt qu’un bain, ne pas laisser  les appareils branchés ou en veille, fermer les robinets
- Produire sa propre énergie pour alimenter les objets du quotidien : vélo/blender ou la machine à laver/vélo, panneaux - solaires, micro éolienne
- Récupérer les eaux de pluie pour l’arrosage
- Récupérer les eaux grises pour le lavage de la voiture 
- Le chauffe-eau solaire individuel et collectif 
- La maison autonome saine 

## CONCEPTION 
- Bâtiments HQE : Haute Qualité Environnementale
- VENTILER : Privilégier la ventilation naturelle à la climatisation 
- ISOLER : Protéger sa maison du rayonnement solaire
- INSERTION  : Construire en s’intégrant dans son environnement, c’est la maison qui s’adapte au terrain et pas l’inverse 
- CONSTRUIRE DURABLEMENT : Matériaux de construction à faible impact carbone 
- NATURE : réintégrer la nature dans l’habitat individuel et collectif pour apporter un meilleur confort thermique et visuel
- Privilégier des produits locaux 

## URBANISME
- Habitat horizontal contre étalement urbain
- Jardins partagés en pied d'immeubles : lien social et autosuffisance alimentaire 
- Echanges de services : “prêter son jardin.com”
- Gestion des déchets : compost collectif, cercle vertueux les déchets alimentaires se recyclent et impact écologique et -économique : moins de déchets moins de taxes moins de déchetterie  
- Habitat minimaliste : les tinny house, se contenter du minimum
- Parkings pour vélos dans les immeubles
- Aides à l’amélioration de l’habitat 

## Produits innovantS
- Bois 
- Isolation : La Watt
- Toit 100% solaire 
- Toiture, ombrage et mur végétal

## Education 
- Formation sur le bien construire et le bien vivre
- Dès le plus jeune âge sensibiliser à l’architecture
- Apprentissage des outils numeriques et citoyens 
- Où prendre conseil ? CAUE, ADEME, DEAL

## Proverbe
Quand le vent du changement se lève,
Les uns construisent des murs, 
les autres des moulins à vent
Proverbe Chinois
